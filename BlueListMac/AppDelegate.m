//
//  AppDelegate.m
//  BlueListMac
//
//  Created by Konstantin Zhulidov on 22/12/13.
//  Copyright (c) 2013 Sioux. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

@end
