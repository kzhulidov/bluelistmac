//
//  AppDelegate.h
//  BlueListMac
//
//  Created by Konstantin Zhulidov on 22/12/13.
//  Copyright (c) 2013 Sioux. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
