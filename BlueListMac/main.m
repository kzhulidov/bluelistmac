//
//  main.m
//  BlueListMac
//
//  Created by Konstantin Zhulidov on 22/12/13.
//  Copyright (c) 2013 Sioux. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
