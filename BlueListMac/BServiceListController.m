//
//  BServiceListController.m
//  BlueListMac
//
//  Created by Konstantin Zhulidov on 27/12/13.
//  Copyright (c) 2013 Sioux. All rights reserved.
//

#import "BServiceListController.h"
#import "BService.h"

@interface BServiceListController ()

@property (nonatomic) IOBluetoothDeviceInquiry *devinq;
@property (nonatomic) NSMutableArray *services; // array of BService
@property (weak) IBOutlet NSButton *startStopScanButton;
@property (weak) IBOutlet NSProgressIndicator *scanProgress;
@property (weak) IBOutlet NSTableView *tableView;

@end

@implementation BServiceListController

- (IOBluetoothDeviceInquiry *)devinq
{
    if (! _devinq) {
        _devinq = [[IOBluetoothDeviceInquiry alloc] initWithDelegate:self];
    }

    return _devinq;
}

- (NSMutableArray *)services
{
    if (! _services) {
        _services = [[NSMutableArray alloc] init];
    }

    return _services;
}

- (void)awakeFromNib
{
    [self.tableView setDataSource:self];
}

- (IBAction)startStopScan:(id)sender
{
    IOReturn ret;

    if ([self.startStopScanButton.title isEqualToString:@"Scan"]) {
        ret = [self.devinq start];
    }
    else {
        ret = [self.devinq stop];
    }

    NSLog(@"Action result is %d", ret);

    if (ret != kIOReturnSuccess) {
        if (ret == kIOReturnNotPermitted) {
            [[self alertNotPermitted] runModal];
        }
        else {
            [[self alertDefault:ret] runModal];
        }
    }
}

- (NSAlert *)alertDefault:(IOReturn)ret
{
    return [NSAlert alertWithMessageText:@"Action failed"
                           defaultButton:nil
                         alternateButton:nil
                             otherButton:nil
               informativeTextWithFormat:@"Error code is (sys=%x, sub=%x, code=%x). Please try again.", err_get_system(ret), err_get_sub(ret), err_get_code(ret)];
}

- (NSAlert *)alertNotPermitted
{
    return [NSAlert alertWithMessageText:@"Action failed"
                           defaultButton:nil
                         alternateButton:nil
                             otherButton:nil
               informativeTextWithFormat:@"Operation not permitted. Please check that Bluetooth device is turned on."];
}

- (void)scanStarted
{
    self.startStopScanButton.title = @"Stop";
    [self.scanProgress startAnimation:self];
}

- (void)scanStopped
{
    self.startStopScanButton.title = @"Scan";
    [self.scanProgress stopAnimation:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.services count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSString *str;
    NSString *identifier = tableColumn.identifier;
    BService *service = [self.services objectAtIndex:row];

    if ([identifier isEqualToString:@"Name"]) {
        str = service.name;
    }
    else if ([identifier isEqualToString:@"RSSI"]) {
        str = [[NSString alloc] initWithFormat:@"%@", service.RSSI];
    }

    return str;
}

#pragma mark - Device discovery

- (void)deviceInquiryStarted:(IOBluetoothDeviceInquiry *)sender
{
    NSLog(@"Discovery started...");

    [self scanStarted];
}

- (void)deviceInquiryComplete:(IOBluetoothDeviceInquiry*)sender error:(IOReturn)error aborted:(BOOL)aborted
{
    NSLog(@"Discovery completed with code %d", error);

    [self scanStopped];
}

- (void)deviceInquiryDeviceFound:(IOBluetoothDeviceInquiry *)sender device:(IOBluetoothDevice *)device
{
    NSLog(@"Discovered %@", device.nameOrAddress);

    BService *service = [[BService alloc] init];
    service.name = device.nameOrAddress;
    service.RSSI = [[NSNumber alloc] initWithChar:device.RSSI];

    [self.services addObject:service];

    [self.tableView reloadData];
}

@end
