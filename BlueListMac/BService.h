//
//  BService.h
//  BlueList
//
//  Created by Konstantin Zhulidov on 22/12/13.
//  Copyright (c) 2013 Sioux. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BService : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *RSSI;

@end
