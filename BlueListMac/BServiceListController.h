//
//  BServiceListController.h
//  BlueListMac
//
//  Created by Konstantin Zhulidov on 27/12/13.
//  Copyright (c) 2013 Sioux. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOBluetooth/IOBluetooth.h>

@interface BServiceListController : NSObject <IOBluetoothDeviceInquiryDelegate, NSTableViewDataSource>

@end
